<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <!-- Brand -->
    <a class="navbar-brand" href="/landing">
        <h1><i class="fab fa-amazon-pay"></i></h1>
    </a>
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link btn btn-success" href="/landing/clean">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn btn-success" href="/addpost">Add Post</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn btn-success" href="/home/logout" onclick="signOut()">LogOut</a>
            </li>
        </ul>
    </div>
</nav>



<div class="container">

    <!-- Page Heading -->
    <h1 class="my-4">
        <small>These are your Blogs</small>
    </h1>

    <div class="row">
        <?php if (!empty($title[0])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[0] ?>
                        </h4>
                        <p class="card-text"><?= $content[0] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[1])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[1] ?>
                        </h4>
                        <p class="card-text"><?= $content[1] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[2])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[2] ?>
                        </h4>
                        <p class="card-text"><?= $content[2] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[3])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[3] ?>
                        </h4>
                        <p class="card-text"><?= $content[3] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[4])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[4] ?>
                        </h4>
                        <p class="card-text"><?= $content[4] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[5])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[5] ?>
                        </h4>
                        <p class="card-text"><?= $content[5] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[6])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[6] ?>
                        </h4>
                        <p class="card-text"><?= $content[6] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!empty($title[7])) { ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <?= $title[7] ?>
                        </h4>
                        <p class="card-text"><?= $content[7] ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <!-- /.row -->

    <!-- Pagination -->
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link" href="/mypost/clean" aria-label="Previous">
                <span aria-hidden="true">&laquo; Previous</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link" href="/mypost" aria-label="Next">
                <span aria-hidden="true">Next &raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>

</div>
<!-- /.container -->
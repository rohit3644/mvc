<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id" content="100222592982-1g9d1rch560fuv3q65c6kfl0ha8cgn43.apps.googleusercontent.com">
    <title>Login</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" href="/public/stylesheets/app.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script>
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            // var Id = profile.getId(); // Do not send to your backend! Use an ID token instead.
            var email = profile.getEmail();
            var id_token = googleUser.getAuthResponse().id_token;
            var index = profile.getEmail().indexOf('@');
            var res = profile.getEmail().substring(0, index)
            var index1 = profile.getName().indexOf(' ');
            var res1 = profile.getName().substring(0, index1)
            // console.log('id:' +
            //     id_token);
            // console.log('Image URL: ' + profile.getImageUrl());
            // console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
            //  window.location = "/user";
            $.post("/signin/google_sign_in", {
                id_token: id_token,
                username: res,
                name: res1,
                password: res,
                email: email
            }, function(result) {
                if (result == -2) {
                    window.location = "/landing";
                } else if (result == 1) {
                    window.location = "/newuser";
                }
            });

        }

        function renderButton() {
            gapi.signin2.render('google_signin', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSignIn
            });
        }
    </script>
    <script>
        $(document).ready(function() {
            window.setInterval(function() {
                $("#username").blur(function() {

                    var username = $(this).val().trim();

                    if (username != '') {

                        $.ajax({
                            url: '/register1/ajaxregister',
                            type: 'post',
                            data: {
                                username: username
                            },
                            success: function(response) {

                                $('#uname_response').html(response).css({
                                    "text-align": "center",
                                    "font-size": "20px"
                                });

                            }
                        });
                    } else {
                        $("#uname_response").html("");
                    }

                });
            }, 2000);
        });
    </script>
</head>
<?php
require_once __DIR__ . "/../../controllers/csrf.php";
?>

<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Sign In</h3>
                </div>
                <div class="card-body">
                    <form method="post" action="/signin/validate_signin">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="username" name="username">

                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="password" name="password">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Login" class="btn float-right login_btn">
                        </div>
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        <h5>
                            Don't have an account?<a class="btn btn-success" href="/signin/sign_up" role="button">Sign Up</a>
                        </h5>
                    </div>
                    <div class="g-signin2" data-onsuccess="onSignIn" id="google_signin" align="center"></div>
                    <br />
                    <div id="error" align="center"><?= $error ?></div>
                </div>
            </div>
        </div>
    </div>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/public/stylesheets/download.png" alt="" height="50px" width="50px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/home/sign_in">Sign In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/signin/sign_up">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="alert alert-primary" role="alert" align="center">
        <h1>Welcome</h1>
        <p><b> This is a Blogging site where you can add your
                blogs by creating an account or read blogs written by other people.
            </b></p>
    </div>
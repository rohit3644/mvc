<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/public/stylesheets/download.png" alt="" height="50px" width="50px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/home/sign_in">Sign In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/signin/sign_up">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <h1 class="my-4">
            <small>Bloggers</small>
        </h1>

        <div class="row">
            <?php if (!empty($title[0])) { ?>
                <div class="col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <?= $title[0] ?>
                            </h4>
                            <p class="card-text"><?= $content[0] ?></p>
                        </div>

                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($title[1])) { ?>
                <div class="col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <?= $title[1] ?>
                            </h4>
                            <p class="card-text"> <?= $content[1] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($title[2])) { ?>
                <div class="col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <?= $title[2] ?>
                            </h4>
                            <p class="card-text"><?= $content[2] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($title[3])) { ?>
                <div class="col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <?= $title[3] ?>
                            </h4>
                            <p class="card-text"><?= $content[3] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($title[4])) { ?>
                <div class="col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <?= $title[4] ?>
                            </h4>
                            <p class="card-text"><?= $content[4] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($title[5])) { ?>
                <div class="col-lg-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <?= $title[5] ?>
                            </h4>
                            <p class="card-text"><?= $content[5] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <h3>Blogger Registration</h3>
                </div>
                <div class="card-body">
                    <form method="post" action="/signup/register">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="username" name="username" id="username">
                        </div>
                        <strong>
                            <div id="uname_response"></div>
                        </strong>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="password" name="password">
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="Confirm password" name="cpassword">
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="name" name="name">
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="email" name="email">
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="role[1 = user, 2 = admin]" name="role">
                        </div>

                        <div class="row">
                            <div class="col">
                                <a class="btn btn-warning" href="/signin" role="button">Sign in</a>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <input type="submit" value="Register" class="btn float-right login_btn">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div id="error"><?= $msg ?></div>
                </div>
            </div>
        </div>
    </div>
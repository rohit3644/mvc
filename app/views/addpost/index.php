<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <!-- Brand -->
    <a class="navbar-brand" href="welcome.php">
        <h1><i class="fab fa-amazon-pay"></i></h1>
    </a>
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link btn btn-success" href="/mypost">My Post</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn btn-success" href="/landing/clean">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn btn-success" href="/home/logout" onclick="signOut()">LogOut</a>
            </li>
        </ul>
    </div>
</nav>
<p>
    <h5 align="center" id="msg"><?= $msg ?></h5>
</p>
<form id="addpost" action="/addpost/post" method="post">
    <div class="jumbotron">
        <div class="form-group">
            <label for="exampleFormControlInput1"><b>Title</b></label>
            <input type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="title">
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1"><b>Content</b></label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="content"></textarea>
        </div>
        <br>
        <input type="submit" class="btn btn-primary" role="button" value="Add">
    </div>
</form>
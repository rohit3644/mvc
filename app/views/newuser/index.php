<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>New User</title>
</head>

<body>
    <div class="alert alert-primary" role="alert" align="center">
        <h1>Welcome, User</h1>
        <p> Username: <?= $_SESSION['username'] ?></p>
        <p> Password: <?= $_SESSION['password'] ?></p>
        <p><b> Please use these credentials for login in future</b></p>
        <p><a class="btn btn-primary" href="/landing" role="button">Dashboard</a></p>
    </div>
</body>

</html>
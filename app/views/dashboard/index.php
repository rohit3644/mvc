<body>

    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <!-- Brand -->
        <a class="navbar-brand" href="welcome.php">
            <h1><i class="fab fa-amazon-pay"></i></h1>
        </a>
        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link btn btn-success" href="/mypost/clean">My Post</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-success" href="/addpost">Add Post</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-success" href="/home/logout" onclick="signOut()">LogOut</a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <h1 class="my-4">
            <small>Popular Blogs</small>
        </h1>

        <!-- Project One -->
        <?php if (!empty($title[0])) { ?>
            <div class="post">
                <h3><?= $title[0] ?></h3>
                <p><?= $content[0] ?></p>
            </div>

            <!-- /.row -->

            <hr>
        <?php } ?>

        <!-- Project Two -->
        <?php if (!empty($title[1])) { ?>
            <div class="post">
                <h3><?= $title[1] ?></h3>
                <p><?= $content[1] ?></p>
            </div>

            <!-- /.row -->

            <hr>
        <?php } ?>

        <!-- Project Three -->
        <?php if (!empty($title[2])) { ?>
            <div class="post">
                <h3><?= $title[2] ?></h3>
                <p><?= $content[2] ?></p>
            </div>

            <!-- /.row -->

            <hr>
        <?php } ?>

        <!-- Project Four -->
        <?php if (!empty($title[3])) { ?>
            <div class="post">
                <h3><?= $title[3] ?></h3>
                <p><?= $content[3] ?></p>
            </div>
            <!-- /.row -->
            <hr>
        <?php } ?>

        <!-- Pagination -->
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="/landing/prev" aria-label="Previous">
                    <span aria-hidden="true">&laquo; Previous</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <!-- <li class="page-item">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li> -->
            <li class="page-item">
                <a class="page-link" href="/landing" aria-label="Next">
                    <span aria-hidden="true">Next &raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>

    </div>
    <!-- /.container -->
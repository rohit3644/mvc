<?php

class signin extends Controller
{

    /*
     * http://localhost/dashboard
     */
    function Index()
    {
        $this->view('signin/header');
        $this->view('signin/index');
        $this->view('signin/footer');
    }

    function validate_signin()
    {
        require_once __DIR__ . "../../../core/classes/data_sanitization.php";
        require_once __DIR__ . "../../../core/classes/csrf_validate.php";
        require_once __DIR__ . "/user.php";
        $ob = new csrf();
        $obj = new User();
        $obj1 = new data();
        $csrf_flag = $ob->csrf($_POST);
        if ($csrf_flag === true) {
            // Proceed to process the form data
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $username = $obj1->check_input($_POST["username"]);
                $password = $_POST["password"];
                $validate = $obj->validate($username, $password);
                if ($validate == 1) {
                    $_SESSION['login'] = $username;
                    header("Location: /landing");
                } else if ($validate == -1) {
                    $error = ["error" => "Invalid Username or Password!!!!"];
                    $this->view('signin/header');
                    $this->view('signin/index', $error);
                    $this->view('signin/footer');
                } else {
                    $error = ["error" => "Enter Username and Password!!!!"];
                    $this->view('signin/header');
                    $this->view('signin/index', $error);
                    $this->view('signin/footer');
                }
            }
        } else {
            var_dump("CSRF ATTACK");
        }
    }

    function sign_up()
    {
        header("Location: /signup");
    }

    function google_Sign_In()
    {
        require  __DIR__ . "/../../vendor/autoload.php";
        require_once __DIR__ . "/user.php";
        $obj = new User();
        $validate = $obj->registration($_POST["username"], $_POST["password"], $_POST["password"], $_POST["name"], $_POST["email"], 1);
        // Get $id_token via HTTPS POST.
        $CLIENT_ID = "100222592982-1g9d1rch560fuv3q65c6kfl0ha8cgn43.apps.googleusercontent.com";

        $client = new Google_Client([
            'client_id' => $CLIENT_ID
        ]);  // Specify the CLIENT_ID of the app that accesses the backend

        $payload = $client->verifyIdToken($_POST['id_token']);
        if ($payload) {
            $userid = $payload['sub'];
            // If request specified a G Suite domain:
            //$domain = $payload['hd'];
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['password'] = $_POST['password'];
            echo $validate;
        } else {
            // Invalid ID token
            echo -10;
        }
        //   echo $_POST['id_token'];
    }

    /*
     * http://localhost/dashboard/subpage/[$parameter]
   */
}

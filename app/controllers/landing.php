<?php

class Landing extends Controller
{
    /*
     * http://localhost/dashboard
     */
    function Index()
    {
        require_once __DIR__ . "/user.php";

        if (isset($_SESSION["login"]) || isset($_SESSION["username"])) {
            $obj1 = new User();
            $count = $obj1->checkCount();

            if ($count == 0) {
                $this->view('dummy/header');
                $this->view('dummy/index');
                $this->view('dummy/footer');
            } else {

                $page_count = $count % 4 == 0 ? $count / 4 : ((int) ($count / 4)) + 1;
                // echo $count;
                // echo $page_count;
                // echo $_SESSION["page"];
                if (!isset($_SESSION["page"])) {
                    $_SESSION["page"] = 1;
                    $_SESSION["offset"] = 0;
                    $_SESSION["limit"] = 4;
                    //echo $_SESSION["offset"];
                } else {
                    $_SESSION["page"] += 1;
                    if ($page_count >= $_SESSION["page"]) {
                        $_SESSION["offset"] += 4;
                        $_SESSION["limit"] = 4;
                    } else {
                        $_SESSION["page"] -= 1;
                    }
                    //echo $_SESSION["offset"];
                }
                $obj = new User();
                $post = $obj->getPost($_SESSION["offset"], $_SESSION["limit"]);
                $this->view('dashboard/header');
                $this->view('dashboard/index', $post);
                $this->view('dashboard/footer');
            }
        } else {
            header("Location: /signin");
        }
    }

    function prev()
    {
        require_once __DIR__ . "/user.php";

        if (isset($_SESSION["login"]) || isset($_SESSION["username"])) {
            if ($_SESSION["page"] > 1) {
                $_SESSION["page"] -= 1;
                $_SESSION["offset"] -= 4;
                $_SESSION["limit"] = 4;
                //echo $_SESSION["offset"];
            } else {
                $_SESSION["page"] = 1;
                $_SESSION["offset"] = 0;
                $_SESSION["limit"] = 4;
                //echo $_SESSION["offset"];
            }
            $obj = new User();
            $post = $obj->getPost($_SESSION["offset"], $_SESSION["limit"]);
            $this->view('dashboard/header');
            $this->view('dashboard/index', $post);
            $this->view('dashboard/footer');
        } else {
            header("Location: /signin");
        }
    }

    function clean()
    {
        unset($_SESSION["page"]);
        header("Location: /landing");
    }
}

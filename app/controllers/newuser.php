<?php
session_start();
class newuser extends Controller
{

    /*
     * http://localhost/
     */
    function Index()
    {
        if (isset($_SESSION["username"])) {
            $this->view('newuser/index');
        } else {
            header("Location: /signin");
        }
    }
}

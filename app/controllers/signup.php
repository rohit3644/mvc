<?php

class signup extends Controller
{

    /*
     * http://localhost/dashboard
     */
    function Index()
    {

        $this->view('signin/header');
        $this->view('signup/index');
        $this->view('signin/footer');
    }


    function register()
    {
        require_once __DIR__ . "../../../core/classes/data_sanitization.php";
        require_once __DIR__ . "/user.php";
        $obj = new data();
        $obj1 = new User();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $username = $obj->check_input($_POST["username"]);
            $password = $_POST["password"];
            $confirm_password = $_POST["cpassword"];
            $name = $obj->check_input($_POST["name"]);
            $email = $_POST["email"];
            $role = $obj->check_input($_POST["role"]);
            $validate = $obj1->registration($username, $password, $confirm_password, $name, $email, $role);
            if ($validate == 1) {
                $_SESSION["regaccess"] = 1;
                header("Location: /register1");
            } else if ($validate == -1) {
                $error = ["msg" => "Passwords doesn't match!"];
                $this->view('signin/header');
                $this->view('signup/index', $error);
                $this->view('signin/footer');
            } else if ($validate == 0) {
                $error = ["msg" => "Please fill all the fields!"];
                $this->view('signin/header');
                $this->view('signup/index', $error);
                $this->view('signin/footer');
            } else {
                $error = ["msg" => "User Exists"];
                $this->view('signin/header');
                $this->view('signup/index', $error);
                $this->view('signin/footer');
            }
        }
    }

    // /*
    //  * http://localhost/dashboard/subpage/[$parameter]
    //  */
    // function subpage($parameter = '', $parameter2 = '')
    // {

    //     $viewData = array(
    //         'parameter' => $parameter,
    //         'parameter2' => $parameter2
    //     );

    //     $this->view('template/header');
    //     $this->view('dashboard/subpage', $viewData);
    //     $this->view('template/footer');
    // }
}

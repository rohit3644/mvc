<?php
class Register1 extends Controller
{

    /*
     * http://localhost/
     */
    function Index()
    {
        if (isset($_SESSION["regaccess"])) {
            $this->view('register/index');
        } else {
            $this->view('signin/header');
            $this->view('signup/index');
            $this->view('signin/footer');
        }
    }

    function ajaxregister()
    {
        if (isset($_POST['username'])) {
            $this->model('register');
            $username = $_POST['username'];
            /*
            $data = [
                $tables_array = ["Login"],
                $select_columns_array = ["Id"],
                $where_condition_array = ["UserName" => $username],
                $operator_condition_array = []
            ];*/

            $tableName = "users";
            $conditions = [
                "select" => ["id"],
                "where" => array(
                    "username" => $username
                ),
                "operators" => []
            ];
            $result = $this->register->register_select($tableName, $conditions);
            $response = "<span style='color: green;'>Available.</span>";
            if (count($result) > 0) {
                $response = "<span style='color: red;'>Not Available.</span>";
            }

            echo $response;
            die;
        }
    }
}

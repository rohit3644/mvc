<?php
class addpost extends Controller
{
    function Index()
    {
        if (isset($_SESSION["login"]) || isset($_SESSION["username"])) {
            $this->view('addpost/header');
            $this->view('addpost/index');
            $this->view('addpost/footer');
        } else {
            header("Location: /signin");
        }
    }

    function post()
    {
        require_once __DIR__ . "/user.php";
        $obj = new User();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST["title"];
            $content = $_POST["content"];
            $blog = $obj->blog($title, $content);
            if ($blog == 1) {
                $msg = ["msg" => "Post added successfully"];
                $this->view('addpost/header');
                $this->view('addpost/index', $msg);
                $this->view('addpost/footer');
            } else {
                $msg = ["msg" => "Please fill all the fields!!!"];
                $this->view('addpost/header');
                $this->view('addpost/index', $msg);
                $this->view('addpost/footer');
            }
        }
    }
}

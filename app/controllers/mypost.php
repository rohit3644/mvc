<?php
class mypost extends Controller
{
    function Index()
    {
        require_once __DIR__ . "/user.php";

        if (isset($_SESSION["login"]) || isset($_SESSION["username"])) {
            $obj1 = new User();
            $count = $obj1->checkCountUser();

            if ($count == 0) {
                $this->view('dummy/header');
                $this->view('dummyMypost/index');
                $this->view('dummy/footer');
            } else {
                $page_count = $count % 8 == 0 ? $count / 8 : ((int) ($count / 8)) + 1;
                if (!isset($_SESSION["mypostpage"])) {
                    $_SESSION["mypostpage"] = 1;
                    $_SESSION["mypostoffset"] = 0;
                    $_SESSION["mypostlimit"] = 8;
                    //echo $_SESSION["offset"];
                } else {
                    $_SESSION["mypostpage"] += 1;
                    if ($page_count >= $_SESSION["mypostpage"]) {
                        $_SESSION["mypostoffset"] += 8;
                        $_SESSION["mypostlimit"] = 8;
                    } else {
                        $_SESSION["mypostpage"] -= 1;
                    }
                    //echo $_SESSION["offset"];
                }
            }
            $obj = new User();
            $mypost = $obj->mypost($_SESSION["mypostoffset"], $_SESSION["mypostlimit"]);
            $this->view('mypost/header');
            $this->view('mypost/index', $mypost);
            $this->view('mypost/footer');
        } else {
            header("Location: /signin");
        }
    }

    function prev()
    {
        require_once __DIR__ . "/user.php";

        if (isset($_SESSION["login"]) || isset($_SESSION["username"])) {
            if ($_SESSION["mypostpage"] > 1) {
                $_SESSION["mypostpage"] -= 1;
                $_SESSION["mypostoffset"] -= 8;
                $_SESSION["mypostlimit"] = 8;
                //echo $_SESSION["offset"];
            } else {
                $_SESSION["mypostpage"] = 1;
                $_SESSION["mypostoffset"] = 0;
                $_SESSION["mypostlimit"] = 8;
                //echo $_SESSION["offset"];
            }
            $obj = new User();
            $post = $obj->mypost($_SESSION["mypostoffset"], $_SESSION["mypostlimit"]);
            $this->view('mypost/header');
            $this->view('mypost/index', $post);
            $this->view('mypost/footer');
        } else {
            header("Location: /signin");
        }
    }

    function clean()
    {
        unset($_SESSION["mypostpage"]);
        header("Location: /mypost");
    }
}

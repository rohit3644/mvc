<?php

/*  Class for Login Validation and Registering new User */

class User extends Controller
{

    public function __construct()
    {
    }

    // Validating Login 

    function Index()
    {
    }

    public function validate($username, $password)
    {
        $this->model('Example');
        $result = 0;
        //if username or password field is empty echo below statement
        if (empty($username) || empty($password)) {
            $result = 0;
        } else {
            $password_hash = hash('sha256', $password);
            /*
            $data = [
                $tables_array = ["Login"],
                $select_columns_array = ["Id"],
                $where_condition_array = ["UserName" => $username, "Password" => $password_hash],
                $operator_condition_array = ["AND"]
            ]; */
            $tableName = "users";
            $conditions = [
                "select" => ["id"],
                "where" => array(
                    "username" => $username,
                    "password" => $password_hash
                ),
                "operators" => ["AND"]
            ];
            // Validating username and password
            $result = $this->Example->validateUser($tableName, $conditions);
            $_SESSION["id"] = (int) $result[0]["id"];
            if (count($result) == 1) {
                $result = 1;
            } else {
                $result = -1;
            }
        }
        return $result;
    }


    // Registering new Users

    public function registration($username, $password, $confirm_password, $name, $email, $role)
    {
        $this->model('register');
        // $this->Register->validateUser($_POST);
        $result = -100000;
        if ($password != $confirm_password) {
            $result = -1;
        } else {

            //if username or password field is empty echo below statement
            if (empty($username) || empty($password) || empty($confirm_password) || empty($name) || empty($email) || empty($role)) {
                $result = 0;
            } else {
                $password_hash = hash('sha256', $password);
                $tableName = "users";
                /*
                $data = [
                    $select_columns_array = ["Id"],
                    $where_condition_array = ["UserName" => $username, "Password" => $password_hash],
                    $special_condition_array = ["AND"]
                ]; */
                $conditions = [
                    "select" => ["id"],
                    "where" => array(
                        "username" => $username,
                        "email" => $email
                    ),
                    "operators" => ["AND"]
                ];
                // Checking if the user already exists
                $result = $this->register->register_select($tableName, $conditions);
                $_SESSION["id"] = $result[0]["id"];
                if (count($result) < 1) {
                    // If the details are unique registering the new user
                    $data_array = [
                        "username" => $username, "password" => $password_hash,
                        "name" => $name, "email" => $email, "role" => $role
                    ];
                    $tableName = "users";
                    $res = $this->register->register_insert($tableName, $data_array);
                    $result = 1;
                } else {
                    $result = -2;
                }
            }
        }
        return $result;
    }

    public function blog($title, $content)
    {
        if (empty($title) || empty($content)) {
            $result = 0;
        } else {
            $this->model('register');
            $tableName = "posts";
            $conditions = [
                "user_id" => $_SESSION["id"], "heading" => $title,
                "content" => $content, "vote_count" => 0
            ];
            $res = $this->register->blog_insert($tableName, $conditions);
            $result = 1;
        }
        return $result;
    }

    public function getPost($offset, $limit)
    {
        $this->model('register');
        $tableName = "posts";
        $conditions = [
            "select" => ["heading", "content"],
            "limit"  => [$offset, $limit],
        ];
        $result = $this->register->getPost($tableName, $conditions);
        $heading = [];
        $content = [];
        foreach ($result as $row) {
            array_push($heading, $row['heading']);
            array_push($content, $row['content']);
        }
        $res = ["title" => $heading, "content" => $content];
        return $res;
    }

    public function checkCountUser()
    {
        $this->model('register');
        $tableName = "posts";
        $conditions = [
            "select" => ["id"],
            "where" => array(
                "user_id" => $_SESSION["id"],
            ),
        ];
        $result = $this->register->getCount($tableName, $conditions);
        return count($result);
    }

    public function checkCount()
    {
        $this->model('Example');
        $tableName = "posts";
        $conditions = ["select" => ["id"]];
        $result = $this->Example->getCount($tableName, $conditions);
        return count($result);
    }

    public function mypost($offset, $limit)
    {
        $this->model('Example');
        $tableName = "posts";
        $conditions = [
            "select" => ["heading", "content"],
            "where" => array(
                "user_id" => $_SESSION["id"],
            ),
            "limit"  => [$offset, $limit],
        ];
        $result = $this->Example->getCount($tableName, $conditions);
        $heading = [];
        $content = [];
        foreach ($result as $row) {
            array_push($heading, $row['heading']);
            array_push($content, $row['content']);
        }
        $res = ["title" => $heading, "content" => $content];
        return $res;
    }
}

<?php

class Home extends Controller
{

    /*
     * http://localhost/login
     */
    function Index()
    {
        require_once __DIR__ . "/user.php";

        $obj1 = new User();
        $count = $obj1->checkCount();

        if ($count == 0 || $count - 6 <= 0) {
            $this->view('dummy/index');
        } else {
            $randomNum = rand(1, $count - 6);
            $post = $obj1->getPost($randomNum, 6);
            $this->view('template/header');
            $this->view('home', $post);
            $this->view('template/footer');
        }
    }

    /*
     * http://localhost/login/log_in
     */
    function sign_In()
    {

        header("Location: /signin");
    }

    /*
     * http://localhost/login/logout
     */
    function Logout()
    {

        $_SESSION = [];
        session_unset();
        session_destroy();
        header('Location: /');
    }
}
